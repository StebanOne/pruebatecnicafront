import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// @ts-ignore
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private URL = 'http://localhost:3000';

  constructor(
    private http: HttpClient,
    private jwtHelper: JwtHelperService) { }

  singin(usuario:any){
    return this.http.post(`${this.URL}/user/singin`, usuario);
  }

  isAuth(): boolean{
    const token = localStorage.getItem('token');
    // @ts-ignore
    if (this.jwtHelper.isTokenExpired(token) || !localStorage.getItem('token')){
      return false;
    }
    return true;
  }
}
