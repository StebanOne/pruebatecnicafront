import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ListarComponent} from "./Persona/listar/listar.component";
import {EditComponent} from "./Persona/edit/edit.component";
import {AddComponent} from "./Persona/add/add.component";
import {LoginComponent} from "./Persona/login/login.component";
import {RoleGuard} from "./guards/role.guard";

const routes: Routes = [
{path:"listar", component: ListarComponent, canActivate:[RoleGuard], data: { expectedRole: "2" } },
{path:"Edit", component: EditComponent},
{path:"Add", component: AddComponent},
{path:"login", component: LoginComponent}

];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
