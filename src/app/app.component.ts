import { Component } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'SuperIntendencia';
  //
  constructor(private router:Router) {}

  listar() {
    this.router.navigate(["listar"]);
  }

  Agregar() {

    this.router.navigate(["Add"]);
  }

  Login() {
    this.router.navigate(["login"]);
  }
}
