import { Component, OnInit } from '@angular/core';
import {Persona} from "../../Modelo/Persona";
import {Router} from "@angular/router";
import {ServiceService} from "../../Service/service.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
    emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  form: any = {};
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';
  persona:Persona= new Persona();

  constructor(private router:Router, private service:ServiceService) { }

  ngOnInit(): void {
  }
  Guardar(){
    this.service.createPersona(this.persona)
      .subscribe(data=>{
        alert("se agrego con exito");
        this.router.navigate(["listar"]);
      })
  }

}
