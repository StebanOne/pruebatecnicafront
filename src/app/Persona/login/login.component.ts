import { Component, OnInit } from '@angular/core';
import {Persona} from "../../Modelo/Persona";
import {Router} from "@angular/router";
import {ServiceService} from "../../Service/service.service";
import {AuthService} from "../../Service/auth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  usuario = {
    name: '',
    password: ''
  };

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  logIn(){
    console.log(this.usuario);
    this.authService.singin(this.usuario).subscribe( (res:any) => {
      alert(res);

      localStorage.setItem('token',res.token);
      this.router.navigate(['listar']);
    })
  }

}
