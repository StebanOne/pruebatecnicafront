import { Component, OnInit } from '@angular/core';
import {ServiceService} from "../../Service/service.service";
import {Persona} from "../../Modelo/Persona";
import {Router} from "@angular/router";

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  persona:Persona = new Persona();
  constructor(private router:Router,private service:ServiceService) { }

  ngOnInit(): void {
    this.Editar();
  }

  Editar(){
    let id=localStorage.getItem("id");
    // @ts-ignore
    this.service.getPersonaId(+id)
      .subscribe(data=>{
        this.persona=data;
      })
  }

  Actualizar(persona:Persona){
    this.service.updatePersona(persona)
      .subscribe(data=>{
        this.persona=data;
        alert("Encuesta Actualizada...!!!");
        this.router.navigate(["listar"]);
      })
  }
}
